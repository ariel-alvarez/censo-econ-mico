# TP Objetos 3: Censo Económico - UNQ #


### Enunciado ###

* [Enunciado](https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnxwcm9ncmFtYWNpb25obXxneDo0NmNmMWI5NjU0N2MyODFk)


* Los edu.unq.obj3.fixtures de los tests se encuentran en /test/scala/EncuestasFixture.scala
* las implementaciones de los servicios pedidos en el enunciado se encuentran en edu.unq.obj3.services.EncuestasServiceComponentImpl

### Arquitectura ###

#### Cake Pattern ####

* Se usa un Cake Pattern para separar homes y servicios (aunque en este momento sólo una implementación de cada uno)

#### Inversión de relaciones ####

Las empresas no conocen directamente a las encuestas, sino que obtienen una home de encuestas del contexto (donde la búsqueda de esa home la hace el compilador en el scope donde se produce la llamada a algún método de la empresa que requiera encuestas, matcheando por tipos). Esto permite trabajar con las colecciones como se haría en un paradigma relacional.

#### DSL ####

Se comienza la construcción de un DSL para las queries definiendo implicits de conversión tales como:


```
#!scala

object EncuestasServiceComponent {
  implicit def intToEncuestaService(unAño:Int)(implicit encuestaService:EncuestasServiceComponent): EncuestasServiceComponent#EncuestaService  =
    encuestaService.service(unAño.toShort)
}
```

El cual, dado un entero, convierte a un servicio restringido a un año igual a dicho entero.
