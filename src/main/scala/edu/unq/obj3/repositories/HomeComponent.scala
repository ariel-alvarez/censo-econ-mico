package edu.unq.obj3.repositories

import edu.unq.obj3.models.Encuesta

class Repository[T](val container: Set[T]) {
  def findAll = container
  def count(f:T => Boolean):Int   = findAll.count(f)
}

trait RepositoryComponentEncuesta {
  val repositoryEncuesta = new Repository[Encuesta](Set.empty[Encuesta])
}
