package edu.unq.obj3.services

import edu.unq.obj3.models.Encuesta.SeqToNumber
import edu.unq.obj3.models.{Empresa, RegistroConocido, _}
import edu.unq.obj3.repositories.{Repository, RepositoryComponentEncuesta}

trait EncuestasServiceComponent {
  this: RepositoryComponentEncuesta =>

  def encuestaService(año:Short):EncuestaService = encuestaService(año, repositoryEncuesta.findAll)
  def encuestaService(encuestas:Iterable[Encuesta]):EncuestaService = new EncuestaService(encuestas.toSet)
  def encuestaService(año:Short, encuestas:Set[Encuesta]):EncuestaService =encuestaService(encuestas.filter(_.año == año))

  class EncuestaService(encuestas:Set[Encuesta]) {

    def totalVentasPorProvincia: Map[Provincia, BigDecimal] = encuestas.foldLeft(Map.empty[Provincia, BigDecimal]){
      (mapa,encuesta) => mapa + (encuesta.departamento.provincia -> (mapa.getOrElse(encuesta.departamento.provincia, 0:BigDecimal)+encuesta.ventas))}

    def cantidadDeRegistrosConTotalTasaDeGananciasMayoresA(cantidad: BigDecimal): Int = encuestas.count(_.tasa > cantidad)

    def cantidadDeRegistrosConTotalDeGananciasMayorA(cantidad: BigDecimal): Int = encuestas.count(_.ganancias > cantidad)

    def cantidadDeRegistrosConTotalDeVentasMayoresA(cantidad: BigDecimal): Int = encuestas.count(_.ventas > cantidad)

    def totalGanancias: BigDecimal = encuestas.sumBy(_.ganancias)

    def totalVentas: BigDecimal = encuestas.sumBy(_.ventas)

    def total(property: Encuesta=>BigDecimal) = encuestas.sumBy(property)

    def totalVentasFold: BigDecimal = encuestas.foldLeft(0:BigDecimal){ (cant,e) => cant+e.ventas}

    def fuentes: Set[Fuente] = encuestas.collect { case r:RegistroConocido => r.empresa.fuente}

    def nombreDeEmpresaConMayoresGanancias = registrosPorEmpresa.map( el => (el._1, el._2.sumBy(_.ganancias))).toSeq.sortBy(-_._2).map(_._1).headOption

    def registrosPorEmpresa: Map[Empresa, Set[RegistroConocido]] = encuestas.collect{case r: RegistroConocido => r}.groupBy(_.empresa)

    def nombresDeEmpresasConVentasMayoresA(cantidad: BigDecimal) = registrosPorEmpresa.filter(_._2.sumBy(_.ventas) > cantidad).map(_._1.nombre)

    def promediando: EncuestaService = new EncuestaService(
      encuestas.collect{case r: RegistroConocido => r}.
        groupBy(_.empresa).
        map(d=>
          (
            d._2.size,
            d._2.reduce(
              (acum, emp) => RegistroConocido(acum.año, acum.ventas+emp.ventas, acum.ganancias+emp.ganancias, acum.fecha, acum.empresa)
            )
          )
        ).
        map( reg => RegistroConocido(reg._2.año, reg._2.ventas/reg._1, reg._2.ganancias/reg._1, reg._2.fecha, reg._2.empresa)).
      toSet
    )

  }

}


object EncuestasServiceComponent {
  implicit def intToEncuestaService(unAño:Int)(implicit encuestaService:EncuestasServiceComponent): EncuestasServiceComponent#EncuestaService  =
    encuestaService.encuestaService(unAño.toShort)

  implicit class EncuestasToService(lista: Iterable[Encuesta])(implicit encuestaService:EncuestasServiceComponent){
    def del(año:Int): EncuestasServiceComponent#EncuestaService = encuestaService.encuestaService(año.toShort, lista.toSet)
    def desde(año:Int): Iterable[Encuesta] = lista.filter(_.año >= año).collect {case r: RegistroConocido => r}
    def hasta(año:Int): EncuestasServiceComponent#EncuestaService = encuestaService.encuestaService(lista.filter(_.año <= año))
    def encontrar: EncuestasServiceComponent#EncuestaService = encuestaService.encuestaService(lista)
  }
  
  implicit class FiltradorToService(filtrador:FiltraEncuesta)(implicit val encuestaService:EncuestasServiceComponent, implicit val repository:Repository[Encuesta]){
    def del(año:Int): EncuestasServiceComponent#EncuestaService = encuestaService.encuestaService(año.toShort, filtrador.encuestas)
    def desde(año:Int): Iterable[Encuesta] = filtrador.encuestas.filter(_.año >= año)
    def encontrar: EncuestasServiceComponent#EncuestaService = encuestaService.encuestaService(filtrador.encuestas)
  }
  
}