package edu.unq.obj3.models

import edu.unq.obj3.repositories.Repository

class Departamento(val nombre:String, val provincia:Provincia) extends Nombrable  with FiltraEncuesta{
  override def encuestas(implicit all: Repository[Encuesta]): Set[Encuesta] = all.findAll.filter(_.departamento == this)
}
class Provincia(val nombre:String) extends Nombrable with FiltraEncuesta {
  override def encuestas(implicit all: Repository[Encuesta]): Set[Encuesta] = all.findAll.filter(_.departamento.provincia == this).toSet
}

class Fuente(val nombre:String, val provincia:Provincia) extends Nombrable with FiltraEncuesta {
  override def encuestas(implicit all: Repository[Encuesta]): Set[Encuesta] = all.findAll
    .collect{ case r:RegistroConocido => r}.filter(_.empresa.fuente == this).toSet
}