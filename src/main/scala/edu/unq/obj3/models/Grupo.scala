package edu.unq.obj3.models

import edu.unq.obj3.repositories.Repository

case class Grupo(departamentos: Departamento*) extends FiltraEncuesta {
  def incluye(provincia:Provincia) = departamentos.exists(_.provincia == provincia)

  def esHomogeneo = departamentos.groupBy(_.provincia).keys.size == 1

  override def encuestas(implicit all: Repository[Encuesta]): Set[Encuesta] = all.findAll.filter(e => departamentos.contains(e.departamento))
}
