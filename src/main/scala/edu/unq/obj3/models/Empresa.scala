package edu.unq.obj3.models

import edu.unq.obj3.repositories.Repository


case class Empresa(nombre: String, departamento: Departamento, fuente:Fuente) extends Nombrable with ConDepartamento with FiltraEncuesta {
  def encuestas(implicit all:Repository[Encuesta]) = all.findAll.collect{case r: RegistroConocido => r }.filter(_.empresa == this).toSet

  def esSolida(implicit all:Repository[Encuesta]) = encuestas forall ( _.tasa > 10 )

  def esSospechosa(implicit all:Repository[Encuesta]) = encuestas exists ( _.tasa > 85 )

  def añosCon( f:(EncuestaOp => Boolean))(implicit all:Repository[Encuesta]) = encuestas.groupBy(_.año).map( col =>
    col._2.foldLeft(EncuestaOp(0,0)) {
      (a:EncuestaOp,b:Encuesta) => EncuestaOp(a.ganancias+b.ganancias,a.ventas+b.ventas)
    }).count(f)

}

case class EncuestaOp(ganancias: Double, ventas: Double ) extends Operable

trait Nombrable {val nombre:String}

trait ConDepartamento {val departamento:Departamento}