package edu.unq.obj3.models

import com.github.nscala_time.time.Imports._
import edu.unq.obj3.models.ChequeoDeConsistenciaConStrategy.EstrategiaDeChequeo
import org.joda.time.DateTime

trait ChequeoDeConsistencia {
  def esConsistente:Boolean = true
}


//     USANDO COMPOSICIÓN DE MIXINES
//---------------------------------------

trait ValidacionVenta extends ChequeoDeConsistencia {
  this: Encuesta =>
  abstract override def esConsistente = super.esConsistente && this.ventas >= 0
}

trait ValidacionFecha extends ChequeoDeConsistencia {
  this: Encuesta =>
  abstract override def esConsistente = super.esConsistente && (
    this.fecha > DateTime.now.withYear(this.año).withMonthOfYear(1).withDayOfMonth(1)
    )
}

trait ValidacionFuente extends ChequeoDeConsistencia {
  this: Encuesta =>
  abstract override def esConsistente = super.esConsistente && (
    this match {
      case r:RegistroAnonimo => true
      case r:RegistroConocido => r.empresa.fuente.provincia == r.departamento.provincia
    }
  )
}

//     USANDO UN STRATEGY
//-----------------------------

trait ChequeoDeConsistenciaConStrategy extends ChequeoDeConsistencia {
  this: Encuesta =>
  var estrategiaDeValidacion: ( Encuesta => Boolean ) = _
  override def esConsistente:Boolean = estrategiaDeValidacion(this)
}

object EstrategiaNula extends EstrategiaDeChequeo {
  override def apply(encuesta: Encuesta): Boolean = true
}

object EstrategiaVenta extends EstrategiaDeChequeo {
  override def apply(encuesta: Encuesta): Boolean = encuesta.ventas >= 0
}

object EstrategiaFecha extends EstrategiaDeChequeo {
  override def apply(encuesta: Encuesta): Boolean =
    encuesta.fecha > DateTime.now.withYear(encuesta.año).withMonthOfYear(1).withDayOfMonth(1)
}

object EstrategiaFuente extends EstrategiaDeChequeo {
  override def apply(encuesta: Encuesta): Boolean = encuesta match {
    case r:RegistroAnonimo => true
    case r:RegistroConocido => r.empresa.fuente.provincia == r.departamento.provincia
  }
}

object ChequeoDeConsistenciaConStrategy {
  type EstrategiaDeChequeo = ( Encuesta => Boolean )

  implicit class ComponerEstrategias(e:EstrategiaDeChequeo){
    def &&(e2:EstrategiaDeChequeo): EstrategiaDeChequeo = (encuesta: Encuesta) => e(encuesta) && e2(encuesta)
  }
}