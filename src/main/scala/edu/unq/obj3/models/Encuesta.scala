package edu.unq.obj3.models

import edu.unq.obj3.repositories.Repository
import org.joda.time.DateTime


trait Operable {
  val ventas:Double; val ganancias:Double
  def tasa: Double = (ganancias/ventas)*100
}

trait UbicableEnTiempo { val año:Short;  val fecha:DateTime}

trait Encuesta extends ConDepartamento with Operable with UbicableEnTiempo

case class RegistroAnonimo(
  año:Short,ventas:Double,ganancias:Double,fecha:DateTime = DateTime.now(),departamento:Departamento
) extends Encuesta

case class RegistroConocido(
  año:Short,ventas:Double, ganancias:Double, fecha:DateTime = DateTime.now(),empresa:Empresa
) extends Encuesta { val departamento = empresa.departamento}

object Encuesta {
  implicit class SeqToNumber(lista: Iterable[Encuesta]) {
    def sumBy(f:Encuesta=>BigDecimal):BigDecimal = lista.map(f).sum
  }

  implicit class FiltradorToNumber(filtrador:FiltraEncuesta)(implicit all:Repository[Encuesta]) {
    def total(f:Encuesta=>BigDecimal):BigDecimal = filtrador.encuestas.map(f).sum
  }
}

trait FiltraEncuesta {
  def encuestas(implicit all:Repository[Encuesta]):Set[Encuesta]
}