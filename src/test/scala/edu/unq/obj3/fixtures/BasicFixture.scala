package edu.unq.obj3.fixtures

import edu.unq.obj3.models.{Departamento, Empresa, Fuente, Provincia}

trait BasicFixture {

}

//PROVINCIAS
object BuenosAires extends Provincia( "Buenos Aires" )
object SantaFe     extends Provincia( "Santa Fe"     )
object Cordoba     extends Provincia( "Córdoba"      )
object Corrientes  extends Provincia( "Corrientes"   )
object SanLuis     extends Provincia( "San Luis"     )

//FUENTES
object INII extends Fuente ( "INII" , BuenosAires )
object IECA extends Fuente ( "IECA" , Corrientes  )

//DEPARTAMENTOS
object Quilmes  extends Departamento ( "Quilmes",  BuenosAires   )
object Rosario  extends Departamento ( "Rosario",  SantaFe       )
object Mercedes extends Departamento ( "Mercedes", Corrientes    )
object Goya     extends Departamento ( "Goya",     Corrientes    )

object EmpresaSospechosa extends Empresa( "Narcofiestas S.A.", Goya    , INII)
object EmpresaSolida     extends Empresa( "Galletitas S.R.L.", Rosario , IECA)
object EmpresaPorQuebrar extends Empresa( "CiberCafes S.A.",   Quilmes , INII)
