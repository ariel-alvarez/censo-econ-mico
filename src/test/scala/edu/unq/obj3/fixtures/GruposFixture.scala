package edu.unq.obj3.fixtures

import edu.unq.obj3.models.Grupo

trait GruposFixture extends BasicFixture {
  val grupoHomogeneo = Grupo(Mercedes, Goya)
  val grupoHeterogeneo = Grupo(Quilmes, Rosario)
  val grupoVacio = Grupo()
}
