package edu.unq.obj3.fixtures

import edu.unq.obj3.models.{Encuesta, RegistroAnonimo, RegistroConocido}
import edu.unq.obj3.repositories.{Repository, RepositoryComponentEncuesta}

trait EncuestasFixture extends BasicFixture { this: RepositoryComponentEncuesta =>

  override val repositoryEncuesta = new Repository[Encuesta](Set(

    //ENCUESTAS ANONIMAS
    RegistroAnonimo( año = 2010, ventas = 100, ganancias = 1   , departamento = Quilmes ),
    RegistroAnonimo( año = 2011, ventas = 100, ganancias = 5   , departamento = Mercedes),
    RegistroAnonimo( año = 2010, ventas = 100, ganancias = 10  , departamento = Quilmes ),
    RegistroAnonimo( año = 2011, ventas = 100, ganancias = 40  , departamento = Rosario ),
    RegistroAnonimo( año = 2010, ventas = 100, ganancias = 80  , departamento = Quilmes ),
    RegistroAnonimo( año = 2011, ventas = 100, ganancias = 85  , departamento = Quilmes ),
    RegistroAnonimo( año = 2010, ventas = 100, ganancias = 90  , departamento = Rosario ),
    RegistroAnonimo( año = 2011, ventas = 100, ganancias = 100 , departamento = Quilmes ),
    RegistroAnonimo( año = 2013, ventas = 101, ganancias = 50  , departamento = Mercedes),
    RegistroAnonimo( año = 2014, ventas = 60,  ganancias = 50  , departamento = Mercedes),
    RegistroAnonimo( año = 2014, ventas = 80,  ganancias = 90  , departamento = Mercedes),
    RegistroAnonimo( año = 2009, ventas = 20,  ganancias = 91  , departamento = Goya    ),
    RegistroAnonimo( año = 2009, ventas = 30,  ganancias = 90  , departamento = Quilmes ),
    RegistroAnonimo( año = 2009, ventas = 40,  ganancias = 92  , departamento = Mercedes),

    //ENCUESTAS DE EMPRESAS CONOCIDAS
    RegistroConocido(empresa = EmpresaSospechosa, año = 2010, ventas = 100, ganancias = 1  ),
    RegistroConocido(empresa = EmpresaSospechosa, año = 2011, ventas = 100, ganancias = 90 ),
    RegistroConocido(empresa = EmpresaSolida,     año = 2010, ventas = 100, ganancias = 50 ),
    RegistroConocido(empresa = EmpresaSolida,     año = 2011, ventas = 100, ganancias = 70 ),
    RegistroConocido(empresa = EmpresaSolida,     año = 2012, ventas = 70,  ganancias = 35 ),
    RegistroConocido(empresa = EmpresaPorQuebrar, año = 2012, ventas = 50,  ganancias = 35 ),
    RegistroConocido(empresa = EmpresaPorQuebrar, año = 2011, ventas = 100, ganancias = 1  ),
    RegistroConocido(empresa = EmpresaPorQuebrar, año = 2013, ventas = 100, ganancias = 5  )

  ))

}
