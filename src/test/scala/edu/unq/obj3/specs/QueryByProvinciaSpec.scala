package edu.unq.obj3.specs

import edu.unq.obj3.fixtures._
import edu.unq.obj3.repositories.RepositoryComponentEncuesta
import edu.unq.obj3.services.EncuestasServiceComponent
import edu.unq.obj3.services.EncuestasServiceComponent._
import org.scalatest._

import scala.language.postfixOps

class QueryByProvinciaSpec extends FlatSpec with Matchers with BeforeAndAfter {

  implicit val services = new EncuestasServiceComponent
                                        with RepositoryComponentEncuesta
                                        with EncuestasFixture

  implicit val encuestas = services.repositoryEncuesta

  "Para una provincia, en un año dado, " should "saber la cantidad de ventas totales" in {
    (Corrientes del 2009 totalVentas) shouldBe 60
  }

  it should "saber la ganancia total" in {
    (Corrientes del 2009 totalGanancias) shouldBe 183
  }

  it should "conocer la cantidad de registros con ventas mayores a un número" in {
    BuenosAires del 2010 cantidadDeRegistrosConTotalDeVentasMayoresA 30 shouldBe 3
  }

  "Para una provincia en cualqueir año" should "conocer la cantidad de registros con ventas mayores a un número" in {
    BuenosAires.encontrar cantidadDeRegistrosConTotalDeVentasMayoresA 30 shouldBe 8
  }

  it should "conocer los nombres de las empresas con ventas mayores a un valor dado (sin repetidos)" in {
    Corrientes.encontrar.nombresDeEmpresasConVentasMayoresA(20) shouldBe List(EmpresaSospechosa.nombre)
  }

}
