package edu.unq.obj3.specs

import edu.unq.obj3.fixtures.GruposFixture
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

class GruposSpec extends FlatSpec with Matchers with BeforeAndAfter with GruposFixture {

  "Un Grupo " should "tener integrantes" in {
    grupoHomogeneo.departamentos should not be empty
  }

  it should "ser homogéneo si todos sus departamentos pertenecen a la misma provincia" in {
    grupoHomogeneo.esHomogeneo shouldBe true
  }

  it should "ser homogéneo si es un grupo vacío" in {
    grupoHomogeneo.esHomogeneo shouldBe true
  }

  it should "ser heterogéneo si tiene al menos dos departamentos de provincias diferentes" in {
    grupoHeterogeneo.esHomogeneo shouldBe false
  }

}
