package edu.unq.obj3.specs

import edu.unq.obj3.fixtures._
import edu.unq.obj3.models.Encuesta._
import edu.unq.obj3.repositories.RepositoryComponentEncuesta
import edu.unq.obj3.services.EncuestasServiceComponent
import org.scalatest._

class DatosPorEmpresaSpec extends FlatSpec with Matchers with BeforeAndAfter {

  implicit val services = new EncuestasServiceComponent
                                        with RepositoryComponentEncuesta
                                        with EncuestasFixture

  implicit val encuestas = services.repositoryEncuesta

  "Para una Empresa " should "saber la cantidad de ventas totales" in {
    EmpresaPorQuebrar total (_.ventas) shouldBe 150
  }

  it should "saber la cantidad de ganancias totales" in {
    EmpresaSolida total (_.ganancias) shouldBe  155
  }

  it should "saber la cantidad de años en los cuales sus ventas superan cierto valor" in {
    EmpresaSolida añosCon (_.ventas > 50) shouldBe  3
  }

  it should "saber la cantidad de años en los cuales sus ganancias superan cierto valor" in {
    EmpresaSolida añosCon (_.ganancias > 40) shouldBe 2
  }

  it should "saber la cantidad de años en los cuales su tasa de ganancia supera cierto valor" in {
    EmpresaSolida añosCon (_.tasa > 40) shouldBe 3
  }


}
