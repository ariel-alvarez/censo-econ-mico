package edu.unq.obj3.specs

import edu.unq.obj3.fixtures._
import edu.unq.obj3.repositories.RepositoryComponentEncuesta
import edu.unq.obj3.services.EncuestasServiceComponent
import edu.unq.obj3.services.EncuestasServiceComponent._
import org.scalatest._

import scala.language.postfixOps

class EncuestasSpec extends FlatSpec with Matchers with BeforeAndAfter {

  implicit val services = new EncuestasServiceComponent
                                        with RepositoryComponentEncuesta
                                        with EncuestasFixture

  implicit val encuestasHome = services.repositoryEncuesta
  val encuestas = services.repositoryEncuesta.findAll

  "Encuesta Service, para un año dado, " should "saber la cantidad de ventas totales" in {
    encuestas del 2013 total (_.ventas) shouldBe 201
  }

  it should "saber la ganancia total para un año" in {
    encuestas del 2013 total (_.ganancias) shouldBe 55
  }

  it should "conocer la cantidad de registros con ventas mayores a un número" in {
    2014 cantidadDeRegistrosConTotalDeVentasMayoresA 70 shouldBe 1
  }

  it should "conocer la cantidad de registros con ganancias mayores a un número dado" in {
    2014 cantidadDeRegistrosConTotalDeGananciasMayorA  70 shouldBe 1
  }

  it should "conocer la cantidad de registros con tasa de ganancia mayor a un número dado" in {
    2011 cantidadDeRegistrosConTotalTasaDeGananciasMayoresA  80 shouldBe 3
  }

  it should "conocer la cantidad de ventas por provincia" in {
    2010 totalVentasPorProvincia BuenosAires shouldBe 300
  }

  it should "conocer los nombres de las empresas con ventas mayores a un valor dado" in {
    2012 nombresDeEmpresasConVentasMayoresA 60 should contain only EmpresaSolida.nombre
  }

  it should "conocer las fuentes que recabaron datos de al menos una empresa" in {
    (2013 fuentes) shouldBe Set(INII)
  }

  it should "conocer la empresa con mayores ganancias" in {
    2011.nombreDeEmpresaConMayoresGanancias should contain (EmpresaSospechosa)
    2013.nombreDeEmpresaConMayoresGanancias should contain (EmpresaPorQuebrar)
    2020.nombreDeEmpresaConMayoresGanancias shouldBe empty
  }

  "Una Empresa" should "ser sólida si todas sus tasas de ganancia superan 10" in {
    EmpresaSolida.     esSolida shouldBe true
    EmpresaPorQuebrar. esSolida shouldBe false
  }

  it should "ser sospechosa si tiene al menos un año con tasa de ganancia mayor a 85" in {
    EmpresaSospechosa.  esSospechosa shouldBe true
    EmpresaSolida.      esSospechosa shouldBe false
  }

}
