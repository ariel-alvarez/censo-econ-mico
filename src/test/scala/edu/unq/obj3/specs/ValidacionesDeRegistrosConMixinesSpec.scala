package edu.unq.obj3.specs

import edu.unq.obj3.fixtures.{BasicFixture, Quilmes}
import edu.unq.obj3.models.{RegistroAnonimo, ValidacionFecha, ValidacionVenta}
import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
 *  Se implementaron las validaciones utilizando mixines y un strategy. En ambos casos, se pueden ir decorando
 *  los registros con diferentes validaciones. En el caso del strategy, la validación puede cambiar en tiempo
 *  de ejecución, mientras que en el caso del mixin, quedará fija. Ambos tienen sus ventajas dependiendo de cómo
 *  quiera usar las validaciones. Si quisiera, por ejemplo, poder cambiar las validaciones en runtime, o construir
 *  los registros con validaciones diferentes en un momento u otro, la implementación con el strategy es más ventajosa.
 *  Ahora bien,si a mi me interesa que la validación sea parte del Tipo de mi registro, donde sé que se trata de una
 *  regla de negocio que debe permanecer inmutable, entonces la implementación con mixines es más ventajosa.
 * */

 class ValidacionesDeRegistrosConMixinesSpec extends FlatSpec with Matchers with BeforeAndAfter with BasicFixture{

  "Un Registro" should "ser consistente si tiene un importe de ventas mayor o igual a cero y usa ValidacionVenta" in {
    val registroConsistente = new RegistroAnonimo( año = 2010, ventas = 100, ganancias = 1   , departamento = Quilmes
    ) with ValidacionVenta
    val registroInconsistente = new RegistroAnonimo( año = 2010, ventas = -1, ganancias = 1   , departamento = Quilmes
    ) with ValidacionVenta

    registroConsistente.esConsistente   shouldBe true
    registroInconsistente.esConsistente shouldBe false

  }

  it should "ser consistente si su año es menor a su fecha de optención y usa ValidacionFecha" in {
    val registroConsistente = new RegistroAnonimo( año = 2010, ventas = 100, ganancias = 1   , departamento = Quilmes,
      fecha = DateTime.now.withYear(2011)
    ) with ValidacionFecha

    val registroInconsistente = new RegistroAnonimo( año = 2010, ventas = 100, ganancias = 1   , departamento = Quilmes,
      fecha = DateTime.now.withYear(2009)
    ) with ValidacionFecha

    registroConsistente.esConsistente   shouldBe true
    registroInconsistente.esConsistente shouldBe false
  }

  it should "puede tener chequeos de validacion compuestos" in {
    val registroConsistente = new RegistroAnonimo( año = 2010, ventas = 100, ganancias = 1   , departamento = Quilmes,
      fecha = DateTime.now.withYear(2011)
    ) with ValidacionFecha with ValidacionVenta

    val registroInconsistente = new RegistroAnonimo( año = 2010, ventas = 100, ganancias = 1   , departamento = Quilmes,
      fecha = DateTime.now.withYear(2009)
    ) with ValidacionFecha with ValidacionVenta

    val registroInconsistente2 = new RegistroAnonimo( año = 2010, ventas = -1, ganancias = 1   , departamento = Quilmes,
      fecha = DateTime.now.withYear(2012)
    ) with ValidacionFecha with ValidacionVenta

    registroConsistente.esConsistente   shouldBe true

    registroInconsistente.esConsistente shouldBe false
    registroInconsistente2.esConsistente shouldBe false
  }




}
