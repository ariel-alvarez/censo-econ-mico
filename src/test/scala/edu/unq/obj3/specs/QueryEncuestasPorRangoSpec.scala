package edu.unq.obj3.specs

import edu.unq.obj3.fixtures._
import edu.unq.obj3.repositories.RepositoryComponentEncuesta
import edu.unq.obj3.services.EncuestasServiceComponent
import edu.unq.obj3.services.EncuestasServiceComponent._
import org.scalatest._

import scala.language.postfixOps

class QueryEncuestasPorRangoSpec extends FlatSpec with Matchers with BeforeAndAfter {

  implicit val services = new EncuestasServiceComponent
                                        with RepositoryComponentEncuesta
                                        with EncuestasFixture

  implicit val encuestasHome = services.repositoryEncuesta
  val encuestas = services.repositoryEncuesta.findAll

  "Encuesta Service, para un rango dado, " should "saber la cantidad de ventas totales" in {
    (encuestas desde 2013 hasta 2014 totalVentas) shouldBe 100
  }

  it should "saber la ganancia total para un año" in {
    (encuestas desde 2011 hasta 2013 totalGanancias) shouldBe 201
  }

  it should "conocer la cantidad de registros con ventas mayores a un número" in {
    encuestas desde 2013 hasta 2014 cantidadDeRegistrosConTotalDeVentasMayoresA 70 shouldBe 1
  }

  it should "conocer la cantidad de registros con ganancias mayores a un número dado" in {
    encuestas desde 2013 hasta 2014 cantidadDeRegistrosConTotalDeGananciasMayorA 70 shouldBe 0
  }

  it should "conocer la cantidad de registros con tasa de ganancia mayor a un número dado" in {
    encuestas desde 2011 hasta 2014 cantidadDeRegistrosConTotalTasaDeGananciasMayoresA  80 shouldBe 1
  }

  it should "conocer la cantidad de ventas por provincia" in {
    (encuestas desde 2009 hasta 2011) totalVentasPorProvincia BuenosAires shouldBe 100
  }

  it should "conocer los nombres de las empresas con ventas mayores a un valor dado" in {
    (encuestas desde 2012 hasta 2013 nombresDeEmpresasConVentasMayoresA 60).toSet shouldBe
      Set(EmpresaPorQuebrar.nombre,EmpresaSolida.nombre)
  }

  it should "conocer las fuentes que recabaron datos de al menos una empresa" in {
    (encuestas desde 2013 hasta 2014 fuentes) shouldBe Set(INII)
  }

  it should "conocer la empresa con mayores ganancias" in {
    (encuestas desde 2011 hasta 2014 nombreDeEmpresaConMayoresGanancias).get shouldBe EmpresaSolida
  }

  it should "calcular con el promedio de los años" in {
    (encuestas desde 2009 hasta 2013 promediando) totalVentasPorProvincia Corrientes shouldBe 100
  }

}
