name := """censo_economico"""

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "org.scalatest"           % "scalatest_2.11" % "2.2.4" % "test",
  "com.github.nscala-time" %% "nscala-time"    % "1.4.0",
  "org.scalaz"             %% "scalaz-core"    % "7.1.0"
)


